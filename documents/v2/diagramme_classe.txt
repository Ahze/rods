@startuml
title Diagramme de classe Modele v2

abstract class Drawable{
	# Drawable(x : int, y : int)
	# Drawable(x : int, y : int, spriteID : int)
	+ draw(g : Graphics)
        + setX(x : int)
        + setY(y : int)
	+ getX() : int
	+ getY() : int
	+ getWidth() : int
	+ getHeigth() : int
}

SpriteBox "1" <-- "1" Drawable : ~spriteBox

Drawable <|-- Entity
Drawable <|-- Map
Drawable <|-- Case

class GameRods{
	+ GameRods()
	+ update()
	+ evoluer(clavier : CClavier, souris : Csouris) : String
	+ draw(g : Graphics)
	+ isOver() : boolean
	+ getMap() : Map
	+ getPlayer() : Player
}

Player "1" <-- "1" GameRods : -player
Mob "*" <-- "1" GameRods: -mobs
Map "1" <-- "1" GameRods : -map

abstract class Entity{
	# vx : int
	# vy : int
	# hp : int
	# Entity(x : int, y : int)
	+ move(direction : Direction)
        + tryToMove(otherElements : ArrayList<Drawable>, directions : Direction[]) : boolean
	+ getHp() : int
}

Direction "1" <- "1" Entity: -direction

Entity <|-- Mob
Entity <|-- Player

class Player{
	- nom : String
	- {static} SpriteBox : SpriteBox
	+ Player(nom : String, x : int, y : int)
	+ setDirection(d : Direction)
	+ draw(g : Graphics)
}

abstract class Mob{
	# Mob(x : int, y : int)
}

class Map{
	# xEntrance : int
	# yEntrance : int
	- SpriteBox : SpriteBox
	+ getNombreCaseHauteur() : int
	+ getNombreCaseLargeur() : int
	+ getWidth() : int
	+ getHeight() : int

}
Map <|-- Labyrinthe

class Labyrinthe{
	+ Labyrinthe(width : int, height : int)
	+ generateEntrance()
	+ draw(g : Graphics)
	+ getCase(x : int, y : int) : Case
	+ getXEntrance() : int
	+ getYEntrance() : int
	+ getNombreCaseHauteur() : int
	+ getNombreCaseLargeur() : int
	+ getWidth() : int
	+ getHeight() : int
	+ toString() : String
}

Case "*" <-- "1" Labyrinthe : -tab_Cases

class Case{
	- wall : boolean
	- {static} SpriteBoxWall : SpriteBox
	- {static} SpriteBoxGround : SpriteBox
	+ isWall() : boolean
	+ toString() : String
}


enum Direction{
	Up
	Down
	Left
	Right
	None
	+ getAllDir() : Direction[]
	+ random() : Direction
	+ toString : toString()
}

class SpriteBox{
	- spriteID : int
	+ SpriteBox(spriteID : int)
	+ collides(d : Drawable) : boolean
	+ collides(s : SpriteBox) : boolean
	+ draw(g : Graphics, x : int, y : int)
	+ getSpriteBoxName() : String
}
@enduml
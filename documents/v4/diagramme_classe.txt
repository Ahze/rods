@startuml

title Diagramme de classe Modele v3

class Game.GameRods{
	+ GameRods()
	+ update()
	+ evoluer(clavier : CClavier, souris : Csouris) : String
	+ getAllElements() : ArrayList<Drawable>
	+ draw(g : Graphics)
	+ isOver() : boolean
	+ getMap() : Map
	+ getPlayer() : Player
}

Entitys.Player "1" <-- "1" Game.GameRods : - {static} player
Entitys.Mob "*" <-- "1" Game.GameRods: - {static} mobs
Maps.WorldMap "1" <-- "1" Game.GameRods : - {static} map

abstract class Sprite.Drawable{
	- drawCollision : boolean
	# Drawable(x : int, y : int)
	# Drawable(x : int, y : int, spriteID : int)
	+ draw(g : Graphics)
    + setX(x : int)
    + setY(y : int)
	+ getX() : int
	+ getY() : int
	+ getWidth() : int
	+ getHeigth() : int
}

Sprite.SpriteBox "1" <-- "1" Sprite.Drawable : ~spriteBox

Sprite.Drawable <|-- Entitys.Entity
Sprite.Drawable <|-- Maps.WorldMap
Sprite.Drawable <|-- Maps.Tile

abstract class Entitys.Entity{
	# vx : int
	# vy : int
	# hp : int
	# Entity(x : int, y : int)
	+ {abstract} update(allElements : ArrayList<Drawable>, map : Map)
    + tryToMove(otherElements : ArrayList<Drawable>, map : Map) : boolean
	+ move(map : Map)
	+ setDirection(direction : Direction)
	+ getHp() : int
}

Entitys.Entity "1" -> "1" Entitys.Direction: -direction

Entitys.Entity <|-- Entitys.Mob
Entitys.Entity <|-- Entitys.Player

class Entitys.Player{
	- nom : String
	- spriteID : int
	+ Player(nom : String, x : int, y : int)
	+ update(allElements : ArrayList<Drawable>, map : Map)
	+ setDirection(d : Direction)
	+ addDirection(d : Direction)
	+ draw(g : Graphics)
}

abstract class Entitys.Mob{
	# Mob(x : int, y : int, spriteID : int)
	+ update(allElements : ArrayList<Drawable>, map : Map)
	+ findDirection(player : Player, maze : Maze) : Direction
}

Entitys.Mob "1" -> "1" Entitys.Pathfinding : -pathfinding

class Entitys.Pathfinding{
	- source : Entity
	- targetTile : Tile
	- firstSourceTile : Tile
	- sourceTiles : ArrayList<Tile>
	- indexes : int[][]
	- {static} target : Entity
	- {static} maze : maze
	+ Pathfinding(source : Entity)
	+ findDirection() : Direction
	+ initializeIndexes()
	+ initializeSource()
	+ indexMaze()
	+ getNextTile() : Tile
	+ aroundingTiles(tile : Tile) : ArrayList<Tile>
	+ getTileByDirection(tile : Tile, direction : Direction) : Tile
	+ getIndexTile(tile : Tile) : int
	+ setIndexTile(tile : Tile, i : int)
	+ {static} setTarget(target : Entity)
	+ {static} setMaze(maze : Maze)	
}

class Maps.WorldMap{
	+Map(spriteID : int)
	+Map(maze : Maze)
	+ draw(Graphics : g)
	+ getMaze() : Maze
	+ xEntrance : int
	+ yEntrance : int
	+ getWidth() : int
	+ getHeight() : int

}
Maps.Maze <- Maps.WorldMap

class Maps.Maze{
	- xEntrance : int
	- yEntrance : int
	+ Maze(width : int, height : int)
	+ findEmptyTile() : Tile
	+ generateEntrance()
	+ draw(g : Graphics)
	+ getTile(x : int, y : int) : Tile
	+ getAllElements() : ArrayList<Drawable>
	+ getXEntrance() : int
	+ getYEntrance() : int
	+ getTileNumberWidth() : int
	+ getTileNumberHeight() : int
	+ getWidth() : int
	+ getHeight() : int
	+ toString() : String
}

Maps.Tile "*" <-- "1" Maps.Maze : -tile_array

class Maps.Tile{
	- wall : boolean
	- xTile : int
	- yTile : int
	+ Tile(x : int, y : int, wall : boolean)
	+ isNextTo(tile : Tile) : boolean
	+ draw(g : Graphics)
	+ getxTile() : int
	+ getyTile() : int
	+ isWall() : boolean
	+ setWall(wall : boolean)
	+ getWidth() : int
	+ getHeight() : int
	+ toString() : String
}


enum Entitys.Direction{
	Up
	Down
	Left
	Right
	UpLeft
	UpRight
	DownLeft
	DownRight
	None
	+ getDx() : int
	+ getDy() : int
	+ getFirstComponent() : Direction
	+ getSecondeComponent() : Direction
	+ {static} getAllDir() : Direction[]
	+ {static} getByDiff(dx : int, dy ; int) : Direction
	+ {static} random() : Direction
	+ toString : toString()
}

class Sprite.SpriteBox{
	- spriteID : int
	- collidable : int
	+ SpriteBox(spriteID : int, x : int, y : int)
	+ SpriteBox(x : int, y : int)
	+ SpriteBox(s : SpriteBox)
	+ draw(g : Graphics, x : int, y : int)
	+ intersects(d : Drawable) : boolean
	+ setX(x : int)
	+ setY(y : int)
	+ setCollidable(c : boolean)
	+ isCollidable() : boolean
	+ getSpriteID() : int
	+ setSpriteID(id : int)
}

Sprite.SpriteBox -|> java.awt.Rectangle

class java.awt.Rectangle{
	+ x : int
	+ y : int
	+ width : int
	+ height : int
	+ intersects(r : Rectangle)
}

abstract class Attack.Attack{
	- damage : int
	- {abstract} update(allElements : ArrayList<Drawable>) : boolean
}

Attack.Attack <-- Entitys.Entity

class Attack.SwordHit{
	- frameCounter : int
	+ SwordHit(x : int, y : int, spriteID : int, damage : int)
	+ update(allElements : ArrayList<Drawable>) : boolean
	+ effectOnCollision(d : Drawable)
}

Attack.Attack <|- Attack.SwordHit

@enduml
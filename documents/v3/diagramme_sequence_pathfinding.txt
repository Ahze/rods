@startuml

title Mob finding Direction with Pathfinding

participant "game:GameRods" as game
participant "m:Mob" as m
participant "pf:Pathfinding" as pf

activate game

create m
game -> m : new(x, y)

activate m
create pf
m -> pf : new(this)
deactivate m

...game starting...

game -> m : update(allElements, map)
activate m

m -> m : findDirection()
m -> pf : findDirecction()

pf -> pf : initializeIndexes()
pf -> pf : initializeSource()
pf -> pf : indexMaze()
pf -> pf : getNextTile()

m <-- pf : Direction

m -> m : newDeplacement(allElements, map)

game <-- m
deactivate m

deactivate game

@enduml
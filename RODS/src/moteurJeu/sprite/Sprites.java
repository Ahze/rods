package moteurJeu.sprite;

import Modele.Game.DessinRods;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.*;

import javax.imageio.ImageIO;

/**
 * classe qui peut charger des images et les afficher
 *
 * <pre>
 * Les images ne sont charges qu'une fois. On doit leur donner un nom au
 * chargement
 *
 * <pre>
 * tout est en statique pour y acceder facilement
 *
 * @author vthomas
 *
 */
public class Sprites {

    /**
     * la map qui stocke les images
     */
    private static Map<Integer, Image> images = loadAllTextures();

    private static HashMap<Integer, Image> loadAllTextures(){
        HashMap<Integer, Image> res = new HashMap<>();
        File spritesFile = new File("data/sprite");
        File[] matchingFiles = spritesFile.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".png");
            }
        });

        for(File f : matchingFiles){
            String spriteName = f.getName();
            spriteName = spriteName.substring(0, spriteName.length()-4);
            int spriteID = Integer.parseInt(spriteName);
            BufferedImage bi = null;
            try {
                bi = ImageIO.read(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Image im;
            if(!spriteName.startsWith("map"))
                im = bi.getScaledInstance(DessinRods.TAILLE_CASE, DessinRods.TAILLE_CASE, Image.SCALE_DEFAULT);
            else
                im = bi;
            res.put(spriteID, im);
            System.out.println("Loaded : "+f.getName()+" as "+spriteID);
        }
        return res;
    }


    /**
     * recupere une image chargee
     *
     * @param nom
     *            image chargee
     */
    public static Image getImage(Integer id) {
        return (images.get(id));
    }

    /**
     * permet de draw l'image sur le graphics
     *
     * @param g
     *            graphics avec lequel draw
     * @param x
     *            position en x
     * @param y
     *            position en y
     */
    public static void dessiner(Graphics g, int id, int x, int y) {
        Image image = getImage(id);
        if (image == null)
            throw new Error("ressource image " + id + " inexistante");
        g.drawImage(image, x, y, null);
    }

    /**
     * permet de draw l'image de ani�re centree sur le graphics
     *
     * @param g
     *            graphics avec lequel draw
     * @param x
     *            position en x
     * @param y
     *            position en y
     */
    public static void dessinerCentre(Graphics g, Integer id, int x, int y) {
        Image image = getImage(id);
        if (image == null)
            throw new Error("ressource image " + id + " inexistante");
        int dx=x-image.getWidth(null)/2;
        int dy=y-image.getHeight(null)/2;

        g.drawImage(image, dx, dy, null);
    }

}

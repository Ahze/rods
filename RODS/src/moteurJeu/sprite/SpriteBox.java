package moteurJeu.sprite;

import java.awt.*;

/**
 * un sprite correspond a une image positionnee a un endroit de la scene
 *
 * @author vthomas
 *
 */
public class SpriteBox extends Rectangle {

	/**
	 * le nom de l'image stock�e dans Images
	 */
	private int spriteID;
	private boolean collidable = true;

	/**
	 * creation du sprite
	 *
	 * @param nom
	 *            nom de l'image
	 */
	public SpriteBox(int spriteID, int x, int y) {
		super(Sprites.getImage(spriteID).getWidth(null), Sprites.getImage(spriteID).getHeight(null));
		this.spriteID = spriteID;
		this.x = x;
		this.y = y;
	}

	public SpriteBox(int x, int y){
		this.x = x;
		this.y = y;
	}

	public SpriteBox(SpriteBox spriteBox){
		this(spriteBox.spriteID, spriteBox.x, spriteBox.y);
	}

	/**
	 * permet de draw le sprite avec un graphics
	 *
	 * @param g
	 *            graphics pour draw
	 */
	public void draw(Graphics g) {
		Sprites.dessiner(g, this.spriteID, this.x, this.y);
	}

	public boolean intersects(Drawable d){
		if(this.collidable && d.getSpriteBox().collidable)
			return this.intersects(d.getSpriteBox());
		else
			return false;
	}

	public boolean isCollidable() {
		return collidable;
	}

	public void setX(int x){
		this.x = x;
	}

	public void setY(int y){
		this.y = y;
	}

	public void setCollidable(boolean collidable) {
		this.collidable = collidable;
	}

	public int getSpriteId() {
		return this.spriteID;
	}

	public void setSpriteID(int spriteID) {
		this.spriteID = spriteID;
	}

}

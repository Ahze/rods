package moteurJeu.sprite;

import java.awt.*;

public abstract class Drawable {

    protected SpriteBox spriteBox;

    protected boolean drawColision = false;

    protected Drawable(int x, int y){
        this.spriteBox = new SpriteBox(x, y);
    }

    protected Drawable(int x, int y, int spriteID){
        this.spriteBox = new SpriteBox(spriteID, x,y);
    }

    public void draw(Graphics g){
        this.spriteBox.draw(g);
    }

    public void effectOnColision(Drawable drawable){}

    public void setX(int x) {
        this.spriteBox.setX(x);
    }

    public void setY(int y) {
        this.spriteBox.setY(y);
    }

    public int getX() {
        return (int)this.spriteBox.getX();
    }

    public int getY() {
        return (int)this.spriteBox.getY();
    }

    public int getWidth(){
        return (int)this.spriteBox.getWidth();
    }

    public int getHeight(){
        return (int)this.spriteBox.getHeight();
    }

    public SpriteBox getSpriteBox() {
        return this.spriteBox;
    }

    public void drawCollision(){
        this.drawColision = true;
    }

}

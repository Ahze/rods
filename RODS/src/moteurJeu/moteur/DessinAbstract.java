package moteurJeu.moteur;

import java.awt.image.BufferedImage;

/**
 * une interface representant la maniere de draw sur un JPanel
 * 
 * @author vthomas
 */
public interface DessinAbstract {

	/**
	 * methode draw a completer. Elle construit une image correspondant au
	 * jeu. Jeu est un attribut de l'afficheur
	 * 
	 * @param image
	 *            image sur laquelle draw
	 */
	public abstract void dessiner(BufferedImage image);

}

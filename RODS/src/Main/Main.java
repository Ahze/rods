package Main;

import Modele.Game.DessinRods;
import Modele.Game.GameRods;
import moteurJeu.moteur.MoteurGraphique;

public class Main {

    public static void main(String[] args) {
        GameRods g = new GameRods();
        DessinRods dr = new DessinRods(g);
        MoteurGraphique mg = new MoteurGraphique(g, dr);



        mg.lancerJeu(1600, 1000, 60);
    }
}

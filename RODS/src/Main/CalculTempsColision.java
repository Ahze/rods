package Main;

import Modele.Game.GameRods;
import Modele.Map.Maze;
import Modele.Map.WorldMap;

public class CalculTempsColision {

    public static void main(String[] args) {
        GameRods game = new GameRods();
        game.setMap(new WorldMap(new Maze(64, 64)));
        long totalTime = 0;
        for(int j = 0; j < 100; j++){

            long start = System.currentTimeMillis();

            for(int i = 0; i < 1000; i++)
                game.getPlayer().update(game.getAllElements());

            long end = System.currentTimeMillis();
            long time = end-start;
            totalTime += time;
        }
        totalTime/= 100;
        System.out.println(((double)totalTime/1000L));
    }

}

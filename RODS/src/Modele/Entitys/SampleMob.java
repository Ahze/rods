package Modele.Entitys;

import moteurJeu.sprite.Drawable;

import java.awt.*;

public class SampleMob extends Mob {

    public SampleMob(int x, int y) {
        super(x, y, 3, 19);
        this.vx = 2;
        this.vy = 2;
    }

    public void draw(Graphics g){
        if(this.mustBeDrawn){
            g.setColor(this.drawColision ? Color.GREEN : Color.BLUE);
            g.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
            this.drawColision = false;
        }
    }


    @Override
    public void attack() {

    }
}

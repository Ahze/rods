package Modele.Entitys;

import Modele.Attack.SwordHit;
import Modele.Game.DessinRods;
import Modele.Map.WorldMap;
import moteurJeu.sprite.Drawable;

import java.awt.*;
import java.util.ArrayList;

public class Player extends Entity {

    private String nom;
    private static int spriteID = 0;
    private Direction lastDirection = Direction.Down;

    public Player(String nNom, int x, int y, int hp){
        super(x, y, hp, spriteID);
        this.nom = nNom;
        this.vx = 5;
        this.vy = 5;
    }

    public boolean update(ArrayList<Drawable> allElements){
        this.newDeplacement(allElements);
        return super.update(allElements);
    }

    @Override
    public void attack() {
        if(this.attack == null){
            this.canMove = false;
            int xAttack = this.getX()+this.lastDirection.getDx()*DessinRods.TAILLE_CASE;
            int yAttack = this.getY()+this.lastDirection.getDy()*DessinRods.TAILLE_CASE;
            this.attack = new SwordHit(xAttack, yAttack, 0, 2);
        }
    }

    public void addDirection(Direction d){
        if(this.movingDirection == Direction.None)
            this.setMovingDirection(d);
        else
            this.setMovingDirection(this.movingDirection.add(d));
    }

    public void setMovingDirection(Direction d){
        if(d != Direction.None)
            this.lastDirection = d;
        this.movingDirection = d;
    }

    public void draw(Graphics g){
        super.draw(g);
        if(this.mustBeDrawn){
            g.setColor(Color.RED);
            g.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
        }
    }

    @Override
    public String toString() {
        return "Player at ("+this.getX()+", "+this.getY()+")\nwidth : "+this.getWidth()+" height : "+this.getWidth();
    }
}

package Modele.Entitys;

import Modele.Map.WorldMap;
import Modele.Map.Pathfinding;
import moteurJeu.sprite.Drawable;

import java.util.ArrayList;

public abstract class Mob extends Entity {

    private Pathfinding pathfinding;

    public Mob(int x, int y, int hp, int spriteID) {
        super(x, y, hp, spriteID);
        this.pathfinding = new Pathfinding(this);
    }

    public boolean update(ArrayList<Drawable> allElements){
        if(this.vulnerable)
            this.findDirection();
        this.newDeplacement(allElements);
        return super.update(allElements);
    }

    public void findDirection(){
        this.movingDirection = pathfinding.findDirection();
    }

    @Override
    public void effectOnColision(Drawable drawable) {
        if(drawable instanceof Player){
            Player player = (Player)drawable;
            player.getKnockbacked(this);
            player.takeDamage(1);
        }
    }

}

package Modele.Entitys;

import Modele.Attack.Attack;
import Modele.Game.GameRods;
import Modele.Map.WorldMap;
import moteurJeu.sprite.Drawable;
import moteurJeu.sprite.SpriteBox;

import java.awt.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public abstract class Entity extends Drawable {

    protected int vx, vy;
    protected int hp;
    protected boolean vulnerable = true, canMove = true, mustBeDrawn = true;
    protected Direction movingDirection = Direction.None;
    protected Attack attack;

    protected Entity(int x, int y, int hp, int spriteID){
        super(x, y, spriteID);
        this.hp = hp;
        this.attack = null;
    }

    public boolean update(ArrayList<Drawable> allElements){
        if(this.attack != null)
            if(this.attack.update(allElements)){
                this.attack = null;
                this.canMove = true;
            }
        return this.isDead();
    }

    public void newDeplacement(ArrayList<Drawable> allElements){
        if(this.canMove) {
            allElements.remove(this);
            if (!this.tryToMove(allElements) && this.movingDirection.isDiagonal()) {
                Direction oldDirection = this.movingDirection;
                this.setMovingDirection(oldDirection.getFirstComponent());
                if (!this.tryToMove(allElements)) {
                    this.setMovingDirection(oldDirection.getSecondComponent());
                    this.tryToMove(allElements);
                }
            }
            allElements.add(this);
        }
    }

    public boolean tryToMove(ArrayList<Drawable> otherElements){
        SpriteBox oldSpritebox = new SpriteBox(this.spriteBox);
        this.move();

        boolean collide = false;

        for(ArrayList<Drawable> list : new ArrayList[]{otherElements, GameRods.getMap().getMaze().getAroundingTiles(this)}){
            for(Drawable d : list){
                if(d.getSpriteBox().intersects(this)){
                    this.effectOnColision(d);
                    d.effectOnColision(this);

                    collide = true;
                    d.drawCollision();
                }
            }
        }
        if(collide)
            this.spriteBox = oldSpritebox;
        return !collide;
    }

    public void move(){
        WorldMap map = GameRods.getMap();
        this.setX(this.getX()+this.vx*this.movingDirection.getDx());
        this.setY(this.getY()+this.vy*this.movingDirection.getDy());
        if(this.getX() < 0)
            this.setX(0);
        else if(this.getX() + this.getWidth() > map.getWidth())
            this.setX(map.getWidth() - this.getWidth());
        if(this.getY() < 0)
            this.setY(0);
        if(this.getY() + this.getHeight() > map.getHeight())
            this.setY(map.getHeight() - this.getHeight());
    }

    public abstract void attack();

    public void takeDamage(int i){
        if(this.vulnerable){
            this.hp -= i;
            this.invicibilityFrame();
        }

    }

    public boolean isDead(){
        return this.hp <= 0;
    }

    public void invicibilityFrame(){
        this.vulnerable = false;
        new Thread(){
            @Override
            public void run() {
                super.run();
                int i = 0;
                while(i < 10){
                    try {
                        TimeUnit.MILLISECONDS.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    i++;
                    Entity.this.mustBeDrawn = !Entity.this.mustBeDrawn;
                }
                Entity.this.vulnerable = true;
            }
        }.start();
    }

    public void getKnockbacked(Drawable drawable){
        if(this.vulnerable) {
            this.canMove = false;
            this.setMovingDirection(Direction.betweenTwoDrawables(drawable, this));
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    try {
                        TimeUnit.MILLISECONDS.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Entity.this.canMove = true;
                }
            }.start();
        }
    }

    public void draw(Graphics g){
        if(this.attack != null)
            this.attack.draw(g);
    }

    public void setMovingDirection(Direction d){
        this.movingDirection = d;
    }


    public int getHp() {
        return hp;
    }

    public boolean canMove() {
        return this.canMove;
    }
}

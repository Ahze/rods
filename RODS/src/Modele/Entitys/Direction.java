package Modele.Entitys;


import moteurJeu.sprite.Drawable;

public enum Direction {
    Up(0, -1),
    Down(0, 1),
    Left(-1, 0),
    Right(1, 0),
    UpLeft(-1, -1),
    UpRight(1, -1),
    DownLeft(-1, 1),
    DownRight(1, 1),
    None(0, 0);

    int dx, dy;

    static final Direction[] allDir = new Direction[]{Up, Down, Left, Right, UpLeft, UpRight, DownLeft, DownRight, None};

    Direction(int dx, int dy){
        this.dx = dx;
        this.dy = dy;
    }

    public Direction getFirstComponent(){
        switch (this){
            case UpLeft:
            case UpRight:
                return Up;
            case DownLeft:
            case DownRight:
                return Down;
            default:
                return None;
        }
    }

    public Direction getSecondComponent(){
        switch (this){
            case UpRight:
            case DownRight:
                return Right;
            case UpLeft:
            case DownLeft:
                return Left;
            default:
                return None;
        }
    }

    public Direction add(Direction d){
        if((this == Up && d == Left) || (d == Up && this == Left))
            return UpLeft;
        if((this == Up && d == Right) || (d == Up && this == Right))
            return UpRight;
        if((this == Down && d == Left) || (d == Down && this == Left))
            return DownLeft;
        if((this == Down && d == Right) || (d == Down && this == Right))
            return DownRight;
        return this == None ? d : this;
    }

    public boolean isDiagonal(){
        switch (this){
            case UpLeft:
            case UpRight:
            case DownLeft:
            case DownRight:
                return true;
            default:
                return false;
        }
    }

    public int getDx() {
        return dx;
    }

    public int getDy() {
        return dy;
    }

    public static Direction[] getAllDir(){
        return allDir;
    }

    public static Direction random(){
        return getAllDir()[(int)(Math.random()*4)];
    }

    public static Direction getByDiff(int dx, int dy){
        for(Direction d : getAllDir()){
            if(d.getDx() == dx && d.getDy() == dy)
                return d;
        }
        return None;
    }

    public static Direction betweenTwoDrawables(Drawable from, Drawable to){
        Direction res = None;
        if(to.getX() > from.getX())
            res = res.add(Right);
        else if(to.getX() < from.getX())
            res = res.add(Left);
        if(to.getY() > from.getY())
            res = res.add(Down);
        else if(to.getY() < from.getY())
            res = res.add(Up);

        return res;
    }

    @Override
    public String toString() {
        switch (this){
            case Up:
                return "Up";
            case Down:
                return "Down";
            case Left:
                return "Left";
            case Right:
                return "Right";
            case None:
                return "None";
            case UpLeft:
                return "UpLeft";
            case UpRight:
                return "UpRight";
            case DownLeft:
                return "DownLeft";
            case DownRight:
                return "DownRight";
            default:
                return "";
        }
    }
}

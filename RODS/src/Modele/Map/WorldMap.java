package Modele.Map;


import Modele.Game.DessinRods;
import moteurJeu.sprite.Drawable;
import moteurJeu.sprite.SpriteBox;

import java.awt.*;
import java.util.ArrayList;

public class WorldMap extends Drawable {

    private SpriteBox sprite;
    private Maze maze;
    private Tile hoveredTile;

    public static boolean drawGrid;

    public WorldMap(int spriteID) {
        super(0, 0);
        sprite = new SpriteBox(spriteID, 0, 0);
    }

    public WorldMap(Maze l){
        super(0, 0);
        this.maze = l;
    }

    @Override
    public void draw(Graphics g) {
        for (Tile[] tileLine : this.maze.getTile_array()) {
            for (Tile tile : tileLine) {
                tile.draw(g);
            }
        }
        if(this.hoveredTile != null)
            this.hoveredTile.drawHovered(g);
    }

    public void searchHoveredTile(int x, int y){
        x += DessinRods.camX;
        y += DessinRods.camY;
        x /= DessinRods.TAILLE_CASE;
        y /= DessinRods.TAILLE_CASE;
        this.hoveredTile = this.maze.getTile(x, y);
    }

    public Maze getMaze() {
        return this.maze;
    }

    public int getWidth(){
        return this.maze.getWidth();
    }

    public int getHeight(){
        return this.maze.getHeight();
    }

    public int getxEntrance() {
        return this.maze.getxEntrance();
    }

    public int getyEntrance() {
        return this.maze.getyEntrance();
    }

    public void setDrawGrid(boolean drawGrid) {
        this.drawGrid = drawGrid;
    }

    public void setHoveredTile(Tile hoveredTile) {
        this.hoveredTile = hoveredTile;
    }
}

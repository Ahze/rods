package Modele.Map;

import Modele.Entitys.Entity;
import Modele.Game.DessinRods;
import moteurJeu.sprite.Drawable;

import java.awt.*;
import java.util.ArrayList;

public class Maze {

    private Tile[][] tile_array;
    private int xEntrance, yEntrance;

    public Maze(int width, int height){
        tile_array = new Tile[width][height];
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                tile_array[j][i] = new Tile(j, i, true);
            }
        }
        this.generateEntrance();
    }

    public void generateEntrance(){
        if((int)(Math.random()*100) < 50){
            this.xEntrance = (int)(Math.random()*100) < 50 ? 0 : getTileNumberWidth()-1;
            this.yEntrance = 1+(int)(Math.random()*(getTileNumberHeight()-1));
        }
        else {
            this.xEntrance = 1+(int)(Math.random()*(getTileNumberWidth()-2));
            this.yEntrance = (int)(Math.random()*100) < 50 ? 0 : getTileNumberHeight()-1;
        }
        tile_array[xEntrance][yEntrance] = new Tile(xEntrance, yEntrance, false);
    }

    public ArrayList<Tile> getAroundingTiles(Entity entity){
        ArrayList<Tile> res = new ArrayList<>();
        int x = entity.getX() / DessinRods.TAILLE_CASE;
        int y = entity.getY() / DessinRods.TAILLE_CASE;

        for(int i = -1; i < 2; i++){
            for(int j = -1; j < 2; j++){
                int nx = x+i;
                int ny = y+j;
                if(this.areCoordsOk(nx, ny))
                    res.add(this.getTile(nx, ny));
            }
        }
        return res;
    }

    public Tile findEmptyTile(){
        int x, y;
        do{
            x = (int)(Math.random()*this.getTileNumberWidth());
            y = (int)(Math.random()*this.getTileNumberHeight());
        }while (this.getTile(x, y).isWall());
        return this.getTile(x, y);
    }

    public void draw(Graphics g){
        for(int y = 0; y < this.getTileNumberHeight(); y++){
            for(int x = 0; x < this.getTileNumberWidth(); x++){
                this.getTile(x, y).draw(g);
            }
        }
    }

    public boolean areCoordsOk(int x, int y){
        return (x >= 0 && x < this.getTileNumberWidth()) && (y >= 0 && y < this.getTileNumberHeight());
    }

    public Tile getTile(int x, int y){
        return tile_array[x][y];
    }

    public int getxEntrance() {
        return xEntrance;
    }

    public int getyEntrance() {
        return yEntrance;
    }

    public int getTileNumberHeight(){
        return this.tile_array[0].length;
    }

    public int getTileNumberWidth(){
        return this.tile_array.length;
    }

    public int getWidth(){
        return getTileNumberWidth()*DessinRods.TAILLE_CASE;
    }

    public int getHeight(){
        return getTileNumberHeight()*DessinRods.TAILLE_CASE;
    }

    public Tile[][] getTile_array() {
        return tile_array;
    }

    public String toString(){
        String res = "";
        for(int i = 0; i < this.getTileNumberHeight(); i++) {
            for (int j = 0; j < this.getTileNumberWidth(); j++) {
                res += tile_array[j][i].isWall() ? "X" : ".";
                res += "\t";
            }
            res += "\n";
        }
        return res;
    }
}

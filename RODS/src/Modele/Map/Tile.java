package Modele.Map;

import Modele.Game.DessinRods;
import Modele.Entitys.Direction;
import moteurJeu.sprite.Drawable;

import java.awt.*;


public class Tile extends Drawable {

    private boolean wall;
    private int xTile, yTile;

    public Tile(int x, int y, boolean wall){
        super(x*DessinRods.TAILLE_CASE, y*DessinRods.TAILLE_CASE, wall ? 1 : 2);
        this.xTile = x;
        this.yTile = y;
        this.wall = wall;
        this.spriteBox.setCollidable(this.wall);
    }

    public void draw(Graphics g) {
        if(this.drawColision){
            g.setColor(Color.GREEN);
            g.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
            this.drawColision = false;
        }
        else {
            this.spriteBox.setSpriteID(wall ? 1 : 2);
            this.spriteBox.draw(g);
        }
        if(WorldMap.drawGrid && !this.wall)
            this.drawGrid(g);
    }

    public void drawGrid(Graphics g){
        g.setColor(new Color(220, 50, 50, 40));
        g.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
        g.setColor(Color.BLACK);
        g.drawLine(this.getX(), this.getY(), this.getX(), this.getY()+DessinRods.TAILLE_CASE/3);
        g.drawLine(this.getX(), this.getY()+DessinRods.TAILLE_CASE-DessinRods.TAILLE_CASE/3, this.getX(), this.getY()+DessinRods.TAILLE_CASE);
        g.drawLine(this.getX(), this.getY(), this.getX()+DessinRods.TAILLE_CASE/3, this.getY());
        g.drawLine(this.getX()+DessinRods.TAILLE_CASE-DessinRods.TAILLE_CASE/3, this.getY(), this.getX()+DessinRods.TAILLE_CASE, this.getY());
    }

    public void drawHovered(Graphics g){
        if(!this.wall){
            g.setColor(new Color(72, 91, 255));
            g.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
        }
    }

    public boolean isNextTo(Tile c){
        for(Direction d : Direction.getAllDir())
            if(this.xTile == c.xTile + d.getDx() && this.yTile == c.yTile + d.getDy())
                return true;
        return false;
    }

    public int getxTile() {
        return xTile;
    }

    public int getyTile() {
        return yTile;
    }

    @Override
    public int getWidth() {
        return DessinRods.TAILLE_CASE;
    }

    @Override
    public int getHeight() {
        return DessinRods.TAILLE_CASE;
    }

    public boolean isWall() {
        return wall;
    }

    public void setWall(boolean wall){
        this.wall = wall;
        this.spriteBox.setCollidable(this.wall);
    }

    @Override
    public String toString() {
        return "("+this.getxTile()+", "+this.getyTile()+") x="+this.getX()+" y="+this.getY()+"\nwidth : "+this.getWidth()+" height : "+this.getWidth();
    }

}

package Modele.Map;


import Modele.Game.DessinRods;
import Modele.Entitys.Direction;
import Modele.Entitys.Entity;
import Modele.Entitys.Player;
import Modele.Entitys.SampleMob;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;


public class Pathfinding {


    private static Entity target;
    private  static Maze maze;

    private Entity source;
    private Tile targetTile, firstSourceTile;
    private ArrayList<Tile> sourceTiles;
    private int[][] indexes;


    public Pathfinding(Entity source) {
        this.source = source;
    }

    public Direction findDirection(){
        //initialize the indexes, the sourceTiles and the the target tile
        this.indexes = new int[maze.getTileNumberWidth()][maze.getTileNumberHeight()];
        this.initializeIndexes();
        this.targetTile = this.maze.getTile(this.target.getX()/DessinRods.TAILLE_CASE,this.target.getY()/DessinRods.TAILLE_CASE);
        this.initializeSource();

        //index the maze from the source to the target
        this.indexMaze();

        //if the source entity can't reach the target, return none
        if(this.getIndexTile(targetTile) == -1)
            return Direction.None;

        //else we search the next tile the source must go on
        Direction dir = Direction.None;
        Tile prochaineCase = this.getNextTile();
        for(Tile c : sourceTiles){
            dir = Direction.getByDiff(prochaineCase.getxTile()-c.getxTile(), prochaineCase.getyTile()-c.getyTile());
            if(dir != Direction.None)
                break;
        }
        //correct the movingDirection if the source is on several tiles
        if(this.sourceTiles.size() > 1 && !dir.isDiagonal()){
            switch (dir){
                case Up:
                case Down:
                    Tile rightTile = this.getTileByDirection(firstSourceTile, Direction.Right);
                    if(this.getIndexTile(rightTile) == 0 && this.getTileByDirection(firstSourceTile, dir.add(Direction.Right)).isWall())
                        dir = Direction.Left;
                    break;
                case Left:
                case Right:
                    Tile downTile = this.getTileByDirection(firstSourceTile, Direction.Down);
                    if(this.getIndexTile(downTile) == 0 && this.getTileByDirection(firstSourceTile, dir.add(Direction.Down)).isWall())
                        dir = Direction.Up;
                    break;
            }
        }
        return dir;
    }

    /**
     * set the index of each tile to -1
     */
    private void initializeIndexes(){
        for(int i = 0; i < this.maze.getTileNumberHeight(); i++){
            for(int j = 0; j < this.maze.getTileNumberWidth(); j++){
                indexes[j][i] = -1;
            }
        }
    }

    /**
     * initialize the sources tiles
     * the source can be on several tiles
     */
    private void initializeSource(){
        this.sourceTiles = new ArrayList<>();
        firstSourceTile = this.maze.getTile(this.source.getX()/DessinRods.TAILLE_CASE,this.source.getY()/DessinRods.TAILLE_CASE);
        this.sourceTiles.add(firstSourceTile);
        for(Direction d : new Direction[]{Direction.Right, Direction.DownRight, Direction.Down}){
            int x = firstSourceTile.getxTile()+d.getDx();
            int y = firstSourceTile.getyTile()+d.getDy();
            if(this.maze.areCoordsOk(x, y))
                if(this.maze.getTile(x, y).getSpriteBox().intersects(this.source.getSpriteBox()))
                    this.sourceTiles.add(this.maze.getTile(x, y));
        }
    }

    /**
     * set the index of all tiles from the sources until reaching the target
     * each tile's index equals n+1, n is the minimum index of the tile arounding it
     */
    public void indexMaze(){
        ArrayList<Tile> frontier = new ArrayList<>();
        for(Tile c : sourceTiles){
            frontier.add(c);
            setIndexTile(c, 0);
        }
        int i = 1;
        do{

            ArrayList<Tile> prochainesCases = new ArrayList<>();

            for(Tile cc : frontier){
                for(Tile c : this.aroundingTiles(cc)) {
                    this.setIndexTile(c, i);
                    prochainesCases.add(c);
                }
            }
            frontier = prochainesCases;
            i++;
        }while (frontier.size() > 0);
    }


    /**
     * Travel the maze from the target to the source using the indexes
     * @return the next tile the source must go on
     */
    public Tile getNextTile(){
        boolean sourceFound = false;

        Tile actualTile = targetTile;
        if(getIndexTile(actualTile) == 1 || getIndexTile(actualTile) == 0)
            return actualTile;

        while(!sourceFound){
            for(Direction d : Direction.getAllDir()){
                int xSide = actualTile.getxTile()+d.getDx();
                int ySide = actualTile.getyTile()+d.getDy();
                if(this.maze.areCoordsOk(xSide, ySide)) {
                    Tile sideTile = this.maze.getTile(xSide, ySide);
                    if (this.getIndexTile(sideTile) == this.getIndexTile(actualTile) - 1) {
                        actualTile = sideTile;
                        break;
                    }
                }
            }
            if(this.getIndexTile(actualTile) == 1)
                sourceFound = true;
        }
        return actualTile;
    }

    /**
     * method that returns all the tiles around the one in parameter, that are indexed -1 and are not a wall
     * @param tile the center tile
     * @return  a list of the arouning tiles
     */
    public ArrayList<Tile> aroundingTiles(Tile tile){
        ArrayList<Tile> retour = new ArrayList<>();
        for(Direction d : Direction.getAllDir()){
            Tile caseAcote = this.getTileByDirection(tile, d);
            if(caseAcote != null) {
                if (getIndexTile(caseAcote) == -1 && !caseAcote.isWall()) {
                    if(d.isDiagonal()){
                        if(!this.getTileByDirection(tile, d.getFirstComponent()).isWall() && !this.getTileByDirection(tile, d.getSecondComponent()).isWall())
                            retour.add(caseAcote);
                    }
                    else
                        retour.add(caseAcote);
                }
            }
        }
        return retour;
    }

    /**
     * return the tile in movingDirection of the one in parameter from the tile in parameter
     * @param tile the source tile
     * @param d the movingDirection to go
     * @return the other tile
     */
    private Tile getTileByDirection(Tile tile, Direction d){
        int x = tile.getxTile() + d.getDx();
        int y = tile.getyTile() + d.getDy();
        if(this.maze.areCoordsOk(x, y))
            return this.maze.getTile(x, y);
        return null;
    }

    /**
     * get the index of the tile in parameters
     * @param tile
     * @return the index of the tile
     */
    private int getIndexTile(Tile tile){
        return indexes[tile.getxTile()][tile.getyTile()];
    }

    /**
     * set the index of a tile
     * @param tile
     * @param i the index
     */
    private void setIndexTile(Tile tile, int i){
        indexes[tile.getxTile()][tile.getyTile()] = i;
    }

    /**
     * set the target
     * @param target
     */
    public static void setTarget(Entity target) {
        Pathfinding.target = target;
    }

    /**
     * set the maze
     * @param maze
     */
    public static void setMaze(Maze maze) {
        Pathfinding.maze = maze;
    }

    /**
     * @return a grid of the index of the tiles
     */
    public String toString(){
        String res = "";
        for(int i = 0; i < this.maze.getTileNumberHeight(); i++){
            for(int j = 0; j < this.maze.getTileNumberWidth(); j++){
                int index = indexes[j][i];
                if(index == -1)
                    res += ("X\t");
                else
                    res += (index+"\t");
            }
            res += "\n";
        }
        return  res;
    }

    public static void main(String[] args) {
        Maze maze = new MazeGenerator().getMaze();
        Player player = new Player("oui", maze.getxEntrance()*DessinRods.TAILLE_CASE, maze.getyEntrance()*DessinRods.TAILLE_CASE, 20);
        Tile caseTroll = maze.findEmptyTile();
        SampleMob sampleMob = new SampleMob(caseTroll.getX()+1, caseTroll.getY()+1);
        Pathfinding pathfinding = new Pathfinding(sampleMob);
        Pathfinding.setMaze(maze);
        Pathfinding.setTarget(player);

        System.out.println(maze);
        System.out.println(pathfinding.findDirection());
        System.out.println(pathfinding);

        int taille_case = 40;

        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(maze.getTileNumberWidth()*taille_case, maze.getTileNumberHeight()*taille_case));
        panel.setLayout(new GridLayout(maze.getTileNumberHeight(), maze.getTileNumberWidth()));
        for(int y = 0; y < maze.getTileNumberHeight(); y++){
            for(int x = 0; x < maze.getTileNumberWidth(); x++){
                int index = pathfinding.getIndexTile(maze.getTile(x, y));
                JLabel b = new JLabel(""+index);
                b.setOpaque(true);
                if(index == 0)
                    b.setBackground(Color.RED);
                else if(index == -1)
                    b.setBackground(Color.BLACK);
                else
                    b.setBackground(new Color(255-index*2, 255-index*2, 255-index*2));
                panel.add(b);
            }
        }

        JFrame window = new JFrame("TEST");
        window.add(panel);
        window.pack();
        window.setVisible(true);
        window.setDefaultCloseOperation(3);


    }

}

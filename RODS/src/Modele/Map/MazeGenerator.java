package Modele.Map;

import Modele.Entitys.Direction;

public class MazeGenerator {

    private Maze maze;

    private Tile actualTile;
    private Direction direction;

    public MazeGenerator(){
        int width = (int)(Math.random()*30)+30;
        int height = (int)(Math.random()*30)+30;
        this.maze = new Maze(width, height);
        this.digMaze();
    }



    public void digMaze(){
        this.findSecondTile();
        int aire = this.maze.getTileNumberHeight()*this.maze.getTileNumberWidth();
        aire /= 4;
        aire *= 3;
        for(int i = 0; i < aire; i++){
            this.actualTile.setWall(false);
            if(!this.movePlace())
                i--;
            this.direction = Direction.random();
        }
    }

    private void findSecondTile(){
        if(this.maze.getxEntrance() == 0)
            this.direction = Direction.Right;
        else if(this.maze.getxEntrance() == this.maze.getTileNumberWidth()-1)
            this.direction = Direction.Left;
        else if(this.maze.getyEntrance() == 0)
            this.direction = Direction.Down;
        else
            this.direction = Direction.Up;
        actualTile = maze.getTile(this.maze.getxEntrance() + this.direction.getDx(), this.maze.getyEntrance() + this.direction.getDy());
    }

    private boolean movePlace(){
        int nx = this.actualTile.getxTile() +this.direction.getDx();
        int ny = this.actualTile.getyTile()+this.direction.getDy();
        if(nx > 0 && nx < this.maze.getTileNumberWidth()-1 && ny > 0 && ny < this.maze.getTileNumberHeight()-1){
            actualTile = this.maze.getTile(nx, ny);
            return true;
        }
        return false;
    }

    public Maze getMaze() {
        return maze;
    }
}

package Modele.Game;

import moteurJeu.moteur.DessinAbstract;

import java.awt.*;
import java.awt.image.BufferedImage;

public class DessinRods implements DessinAbstract {

    private GameRods game;
    public static int TAILLE_CASE = 50, camX, camY;

    public DessinRods(GameRods game){
        this.game = game;
    }

    @Override
    public void dessiner(BufferedImage image) {
        Graphics g = image.getGraphics();
        int minXOffset = 0, maxXOffset = game.getMap().getWidth() - image.getWidth();
        int minYOffset = 0, maxYoffset = game.getMap().getHeight() - image.getHeight();

        camX = game.getPlayer().getX() - image.getWidth()/2;
        camY = game.getPlayer().getY() - image.getHeight()/2;

        if(camX < minXOffset)
            camX = minXOffset;
        else if(camX > maxXOffset)
            camX = maxXOffset;
        if(camY < minYOffset)
            camY = minYOffset;
        else if(camY > maxYoffset)
            camY = maxYoffset;
        g.translate(-camX, -camY);
        game.draw(g);

    }
}

package Modele.Game;

import Modele.Map.*;
import Modele.Entitys.Direction;
import Modele.Entitys.Player;
import Modele.Entitys.Mob;
import Modele.Entitys.SampleMob;
import moteurJeu.moteur.CClavier;
import moteurJeu.moteur.CSouris;
import moteurJeu.moteur.JeuAbstract;
import moteurJeu.sprite.Drawable;

import java.awt.*;
import java.util.ArrayList;

public class GameRods implements JeuAbstract {

    private static WorldMap worldMap;

    private static Player player;
    private static ArrayList<Mob> mobs;


    public GameRods(){
        Maze l = new MazeGenerator().getMaze();
        this.worldMap = new WorldMap(l);
        this.player = new Player("Ahze", this.worldMap.getxEntrance()*DessinRods.TAILLE_CASE, this.worldMap.getyEntrance()*DessinRods.TAILLE_CASE, 20);
        Pathfinding.setTarget(this.player);
        Pathfinding.setMaze(this.worldMap.getMaze());
        mobs = new ArrayList<>();
        Tile c = this.worldMap.getMaze().findEmptyTile();
        mobs.add(new SampleMob(c.getX(), c.getY()));
    }

    public void draw(Graphics g){
        this.worldMap.draw(g);
        player.draw(g);
        for(Mob mob : mobs)
            mob.draw(g);
    }

    @Override
    public String update(CClavier clavier, CSouris souris) {
        if(clavier.isPressed('C'))
            this.player.getSpriteBox().setCollidable(!this.player.getSpriteBox().isCollidable());

        if(this.player.canMove()) {
            player.setMovingDirection(Direction.None);
            if (clavier.isPressed('Z'))
                player.addDirection(Direction.Up);
            else if (clavier.isPressed('S'))
                player.addDirection(Direction.Down);
            if (clavier.isPressed('Q'))
                player.addDirection(Direction.Left);
            else if (clavier.isPressed('D'))
                player.addDirection(Direction.Right);
            if(clavier.isPressed(' '))
                this.player.attack();
        }

        if(souris.isPressed()){
            this.worldMap.setDrawGrid(true);
            this.worldMap.searchHoveredTile(souris.getX(), souris.getY());
        }
        else {
            this.worldMap.setDrawGrid(false);
            this.worldMap.setHoveredTile(null);
        }
        ArrayList<Drawable> allElements = this.getAllElements();

        player.update(allElements);

        for(int i = 0; i < mobs.size(); i++) {
            if(mobs.get(i).update(allElements)){
                //the mob is dead, remove it from the list
                mobs.remove(mobs.get(i--));
            }
        }

        return null;
    }

    public ArrayList<Drawable> getAllElements(){
        ArrayList<Drawable> list = new ArrayList<>();
        list.add(this.player);
        for (Mob m : this.mobs)
            list.add(m);
        return list;
    }

    @Override
    public boolean isOver() {
        return false;
    }

    public void setMap(WorldMap map){
        this.worldMap = map;
    }

    public static WorldMap getMap(){
        return worldMap;
    }

    public static Player getPlayer() {
        return player;
    }

    public ArrayList<Mob> getMobs() {
        return this.mobs;
    }
}


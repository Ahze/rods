package Modele.Attack;

import Modele.Entitys.Mob;
import moteurJeu.sprite.Drawable;

import java.util.ArrayList;

public class SwordHit extends Attack{

    private int frameCounter;

    public SwordHit(int x, int y, int spriteID, int damage) {
        super(x, y, spriteID, damage);
        this.frameCounter = 15;
    }

    @Override
    public boolean update(ArrayList<Drawable> allElements) {
        allElements.remove(this);
        for(Drawable drawable : allElements){
            if(this.getSpriteBox().intersects(drawable))
                this.effectOnColision(drawable);
        }
        allElements.add(this);
        return frameCounter-- == 0;
    }

    public void effectOnColision(Drawable drawable) {
        if(drawable instanceof Mob){
            Mob mob = (Mob)drawable;
            mob.getKnockbacked(this);
            mob.takeDamage(this.damage);
        }
    }

}

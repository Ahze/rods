package Modele.Attack;

import moteurJeu.sprite.Drawable;

import java.awt.*;
import java.util.ArrayList;

public abstract class Attack extends Drawable {

    protected int damage;

    protected Attack(int x, int y, int spriteID, int damage) {
        super(x, y, spriteID);
        this.damage = damage;
    }

    public abstract boolean update(ArrayList<Drawable> allElements);

    public void draw(Graphics g){
        g.setColor(Color.GRAY);
        g.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    }

}

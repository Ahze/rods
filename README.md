Projet par AhzeLeSteak

# Itération n°1
- Affichage de sprites
    - Chaque objet de type Character et Map possède un spriteID
- Commande clavier et souris
    - Les touches 'zqsd' font bouger le personage

# Itération 1.5
- Réorganisation des classes
	- Utilisation de la classe Sprite
- La map peut aussi etre un labyrinthe généré aléatoirement

# Itération n°2
- Collision entre le joueur et les murs

# Itération n°3
- Implémentation du pathfinding pour les monstres
- La touche C permet d'activer ou non la collision du joueur

# Itération n°4
- Ajout de points de vie aux entités
- Appuyer sur espace fait des dégats aux unités devant soit
- Les unités qui se prennent des dégats subissent un knockback

# Itération n°5
- Affichage d'une grille lorsque le clic gauche est pressé
- Affichage d'une texture sur la case du curseur lorsque le clic est pressé
- Infliger des dégats au montre sur la case lorsqu'on relache le clic